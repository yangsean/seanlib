Pod::Spec.new do |s|

s.name         = "seanlib"
s.version      = "0.0.1"
s.summary      = "seanlib is a image cache library for network type."
s.description  = "seanlib 是一个根据网络环境 加载合适图片的工具"
s.homepage     = "https://bitbucket.org/yangsean/seanlib"
s.license      = "MIT"
s.author       = { "yanghonglei" => "272789124@qq.com" }
s.platform     = :ios, "7.0"
s.source       = { :git => "https://yangsean@bitbucket.org/yangsean/seanlib.git" }
s.source_files  = "Pod/Classes/**/*.{h,c,m,swift}"
s.resources = "Pod/Classes/**/*.{xib,nib,plist}"
s.resource_bundles = {
'seanlib' => ['Pod/Assets/**/*.png']
}

s.requires_arc = true
# s.dependency "JSONKit", "~> 1.4"

end